# Generated by Django 2.1 on 2019-08-12 08:44

from django.db import migrations, models
import pages.models


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0002_page_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='page',
            name='image',
            field=models.ImageField(blank=True, default='no-img', null=True, upload_to=pages.models.custom_upload_to, verbose_name='Imagen'),
        ),
    ]
