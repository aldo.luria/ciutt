from django import template
from services.models import Service, Inscription, Group, User, Post

register = template.Library()

@register.simple_tag
def get_service_list():
    services = Service.objects.all()
    return services
    
@register.simple_tag
def get_inscription_list():
    inscriptions = Inscription.objects.all()
    return inscriptions

@register.filter(name='has_group')
def has_group(user, group_name): 
    group = Group.objects.get(name=group_name) 
    return True if group in user.groups.all() else False

@register.simple_tag
def get_posts_list():
    posts = Post.objects.all()
    return posts