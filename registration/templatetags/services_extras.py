from django import template
from services.models import Inscription, Post

register = template.Library()

@register.simple_tag
def get_inscription_list():
    inscriptions = Inscription.objects.all()
    return inscriptions

@register.simple_tag
def get_posts_list():
    posts = Post.objects.all()
    return posts